#### Run project
```
    python -m venv ./.venv
    ./.venv/bin/activate
    pip install poetry
    poetry install
    docker-compose up -d
    cd src
    python manage.py migrate
    python manage.py runserver
```

#### Celery
```
    celery -A share_service beat
    celery -A share_service worker -l INFO
    flower -A share_service --port=5555
```

#### Tests
```
    cd src
    coverage run -m pytest 
    coverage report --rcfile=.coveragerc
```

#### Linters
```
    cd src
    flake8
    black .
    isort .
```

#### Environment variables
###### To enable variables
1. Create copy of share_service/example.env to share_service/local.env
2. Paste values from repository ci/cd variables

###### To add new variables
1. Write new key into share_service/example.env
2. Create new key/value pair in repository ci/cd variables
3. Export variable to env file in .gitlab-ci.yml in before_script block

#### Backup
###### To restore backup
1. If you don’t have access to a dropped database create a new one
2. Run /bin/sh backup/restore.sh [host] [port] [db_name] [user] [password] 

###### To make backup
1. Run /bin/sh backup/backup.sh [host] [port] [db_name] [user] [password 

###### Notes
* Postgres-client which version corresponds to db version and s3cmd must be installed and configured
* Backup happens every day ay 23:59

