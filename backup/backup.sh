#!/bin/bash

# PREREQUISITES
# install s3cmd
# and postgres-client which version corresponds to db version

DB_HOST=$1
DB_PORT=$2
DB_NAME=$3
DB_USER=$4
DB_PASSWORD=$5

BUCKET_NAME='share-service-backups'

TIMESTAMP=$(date +%d-%m-%YT%H-%M-%S)
TEMP_FILE=$(mktemp tmp.XXXXXXXXXX)
S3_FILE="s3://$BUCKET_NAME/$TIMESTAMP.dump"

PGPASSWORD=$DB_PASSWORD pg_dumpall -h $DB_HOST -p $DB_PORT -U $DB_USER -l $DB_NAME > $TEMP_FILE
echo "dump has been created to file ${TEMP_FILE}"

s3cmd put $TEMP_FILE $S3_FILE
echo "dump has been moved to s3"

rm "$TEMP_FILE"
echo "dump file ${TEMP_FILE} has been removed from os"