from rest_framework.routers import DefaultRouter

from main.views.auth import AuthViewSet
from main.views.rating import RatingViewSet
from main.views.user import UserViewSet

router = DefaultRouter(trailing_slash=True)
router.register(r"user", UserViewSet, basename="user")
router.register("rating", RatingViewSet, basename="ratting")
router.register("auth", AuthViewSet, basename="auth")

main_urls = [
    *router.urls,
]
