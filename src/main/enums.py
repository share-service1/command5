from django.db import models
from django.utils.translation import gettext_lazy as _


class TokenEnum(models.TextChoices):
    EMAIL_CONFIRMATION = _("EMAIL CONFIRMATION")
    FORGOT_PASSWORD = _("FORGOT_PASSWORD")
