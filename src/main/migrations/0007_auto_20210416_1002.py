# Generated by Django 3.1.7 on 2021-04-16 10:02

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("main", "0006_merge_20210416_0708"),
    ]

    operations = [
        migrations.AddField(model_name="user", name="rating_count", field=models.PositiveIntegerField(default=0),),
        migrations.AddField(
            model_name="user",
            name="rating_score",
            field=models.DecimalField(decimal_places=2, default=0, max_digits=4),
        ),
        migrations.CreateModel(
            name="Rating",
            fields=[
                ("id", models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                ("rating", models.PositiveSmallIntegerField()),
                (
                    "who",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, related_name="rates", to=settings.AUTH_USER_MODEL
                    ),
                ),
                (
                    "whom",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, related_name="ratings", to=settings.AUTH_USER_MODEL
                    ),
                ),
            ],
            options={"unique_together": {("who", "whom")},},
        ),
    ]
