from storages.backends.s3boto3 import S3Boto3Storage

from share_service import settings


class MediaStorage(S3Boto3Storage):
    location = settings.AWS_MEDIA_LOCATION
    file_overwrite = False
