from functools import partial

from django.db import transaction
from django.db.models.signals import post_delete, post_save, pre_delete, pre_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token

from main.models import Rating, User
from share_service import settings


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


def update_user_rating(user_id, rating, incr_or_decr):
    with transaction.atomic():
        user = User.objects.select_for_update().get(id=user_id)

        old_rating_count = user.rating_count
        if user.rating_count > 0 and incr_or_decr == -1 or incr_or_decr == 1:
            user.rating_count += incr_or_decr
        new_rating_count = user.rating_count

        if new_rating_count == 0:
            user.rating_score = 0
        else:
            user.rating_score = (user.rating_score * old_rating_count + rating * incr_or_decr) / new_rating_count
        user.save()


@receiver(post_save, sender=Rating)
def incr_user_rating(sender, instance, created, **kwargs):
    if created:
        update_user_rating(instance.whom.id, instance.rating, 1)


@receiver(post_delete, sender=Rating)
def decr_user_rating(sender, instance, **kwargs):
    update_user_rating(instance.whom.id, instance.rating, -1)


@receiver(pre_save, sender=Rating)
def change_user_rating(sender, instance, **kwargs):
    if not instance.pk:
        return

    old_instance = sender.objects.get(who=instance.who, whom=instance.whom)

    # TODO: optimize user getting
    with transaction.atomic():
        update_user_rating(instance.whom.id, old_instance.rating, -1)
        update_user_rating(instance.whom.id, instance.rating, 1)


def delete_file_field_on_action(sender, instance, file_field_names, **kwargs):
    def delete_file(instance_, field_name_):
        file_field = getattr(instance_, field_name_)

        if not file_field:
            return

        file_field.delete(save=False)

    if not instance.pk:
        return

    try:
        old_instance = sender.objects.get(pk=instance.pk)
    except sender.DoesNotExist:
        return

    for field_name in file_field_names:
        delete_file(old_instance, field_name)


delete_user_file_fields = partial(delete_file_field_on_action, file_field_names=("avatar",))

pre_save.connect(delete_user_file_fields, sender=User)
pre_delete.connect(delete_user_file_fields, sender=User)
