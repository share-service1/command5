from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from rest_framework import serializers
from rest_framework.fields import empty

from main.models import Token


class TokenSerializer(serializers.ModelSerializer):
    class Meta:
        model = Token
        fields = ("value",)

    def __init__(self, token_type, instance=None, data=empty, **kwargs):
        super().__init__(instance, data, **kwargs)
        self.token_type = token_type

    def validate(self, attrs):
        attrs = super().validate(attrs)

        try:
            token = Token.objects.get(
                value=attrs["value"], token_type=self.token_type, expiration_date__gt=timezone.now()
            )
        except Token.DoesNotExist:
            raise serializers.ValidationError(_("Invalid token"))

        self.instance = token

        return attrs
