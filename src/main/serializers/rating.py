from rest_framework import serializers

from main.models import Rating


# TODO: unique together check
class RatingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rating
        fields = (
            "whom",
            "rating",
        )
        extra_kwargs = {"rating": {"min_value": 1, "max_value": 5,}}

    def create(self, validated_data):
        validated_data["who"] = self.context["request"].user

        return super(RatingSerializer, self).create(validated_data)
