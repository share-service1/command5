import os

import pytest
from rest_framework import status

from main.serializers.user import UserSerializer
from main.tests.api.services.user import (
    create_temp_image,
    get_writeable_serializer_fields,
)
from main.tests.factories.user_factory import UserFactory


@pytest.mark.django_db
def test_get_current_user_success(api_client):
    user = UserFactory.create()
    api_client.force_authenticate(user=user)

    r = api_client.get(path="/user/")

    assert r.status_code == status.HTTP_200_OK
    assert r.json()["email"] == user.email


@pytest.mark.django_db
def test_get_current_user_fail(api_client):
    r = api_client.get(path="/user/")
    assert r.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.django_db
def test_get_user_success(api_client):
    user_1 = UserFactory.create()
    user_2 = UserFactory.create()
    api_client.force_authenticate(user=user_1)

    r = api_client.get(path=f"/user/{user_2.pk}/")
    assert r.status_code == status.HTTP_200_OK
    assert r.json()["email"] == user_2.email


@pytest.mark.django_db
def test_get_user_fail(api_client):
    user = UserFactory.create()
    r = api_client.get(path=f"/user/{user.pk}/")

    assert r.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.django_db
def test_upload_user_avatar_success(api_client):
    user = UserFactory.create()
    api_client.force_authenticate(user=user)

    avatar_suffix = ".jpg"
    avatar_file = create_temp_image(avatar_suffix)
    r = api_client.post(path="/user/avatar/", data={"avatar": avatar_file}, format="multipart")

    assert r.status_code == status.HTTP_200_OK

    r_data = r.json()

    avatar_file_name = os.path.split(avatar_file.name)[1]
    avatar_file_name_thumbnail = avatar_file_name.split(".")[0] + f".thumbnail{avatar_suffix}"

    assert avatar_file_name in r_data["avatar"]["origin"]
    assert avatar_file_name_thumbnail in r_data["avatar"]["thumbnail"]


@pytest.mark.django_db
def test_upload_user_avatar_fail(api_client):
    avatar_suffix = ".jpg"
    avatar_file = create_temp_image(avatar_suffix)
    r = api_client.post(path="/user/avatar/", data={"avatar": avatar_file}, format="multipart")

    assert r.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.django_db
def test_patch_user_success(api_client):
    user = UserFactory.create()
    temp_user = UserFactory.create()
    temp_user_dict = temp_user.__dict__
    user_dict_to_update = dict()

    writable_serializer_fields = get_writeable_serializer_fields(UserSerializer)

    for field in writable_serializer_fields:
        user_dict_to_update[field] = temp_user_dict[field]

    api_client.force_authenticate(user=user)

    r = api_client.patch(path=f"/user/{user.pk}/", data=user_dict_to_update, format="json")

    assert r.status_code == status.HTTP_200_OK

    r_data = r.json()

    for field in writable_serializer_fields:
        assert str(r_data[field]) == str(temp_user_dict[field])


@pytest.mark.django_db
def test_user_patch_fail(api_client):
    user = UserFactory.create()
    temp_user = UserFactory.create()
    temp_user_dict = temp_user.__dict__
    user_dict_to_update = dict()

    writable_serializer_fields = get_writeable_serializer_fields(UserSerializer)

    for field in writable_serializer_fields:
        user_dict_to_update[field] = temp_user_dict[field]

    r = api_client.patch(path=f"/user/{user.pk}/", data=user_dict_to_update, format="json")
    assert r.status_code == status.HTTP_401_UNAUTHORIZED

    api_client.force_authenticate(user=user)
    r = api_client.patch(path=f"/user/{temp_user.pk}/", data=user_dict_to_update, format="json")
    assert r.status_code == status.HTTP_403_FORBIDDEN
