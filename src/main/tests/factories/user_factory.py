import factory
from factory import fuzzy
from factory.django import DjangoModelFactory

from main.models import User


class UserFactory(DjangoModelFactory):
    email = factory.LazyAttribute(lambda a: f"{a.first_name}.{a.last_name}@example.com".lower())
    first_name = factory.Faker("first_name")
    last_name = factory.Faker("last_name")
    username = email
    is_email_verified = False
    rating_count = fuzzy.FuzzyInteger(low=1)
    rating_score = fuzzy.FuzzyDecimal(low=0)

    class Meta:
        model = User
