from django.contrib.auth import authenticate
from django.utils.translation import gettext_lazy as _
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from main.models import User
from main.serializers.auth import (
    ConfirmEmailAPISerializer,
    ConfirmEmailSerializer,
    ForgotPasswordSerializer,
    SetNewPasswordAPISerializer,
    SetNewPasswordSerializer,
    UserAuthOutputSerializer,
    UserSignInSerializer,
    UserSignUpSerializer,
)
from main.services.emails import send_confirm_email, send_forgot_email


class AuthViewSet(GenericViewSet):
    serializer_class = UserAuthOutputSerializer
    permission_classes_by_action = {"send_email_confirmation": (IsAuthenticated,)}

    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_classes_by_action[self.action]]
        except KeyError:
            return super().get_permissions()

    @swagger_auto_schema(
        request_body=UserSignUpSerializer(), responses={200: UserAuthOutputSerializer()},
    )
    @action(methods=["POST"], detail=False)
    def sign_up(self, request):
        serializer = UserSignUpSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()

        send_confirm_email(request, user)

        return Response(self.serializer_class(user).data, status=status.HTTP_201_CREATED)

    @swagger_auto_schema(
        request_body=UserSignInSerializer(), responses={200: UserAuthOutputSerializer()},
    )
    @action(methods=["POST"], detail=False)
    def sign_in(self, request):
        serializer = UserSignInSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        user = authenticate(email=serializer.validated_data["email"], password=serializer.validated_data["password"])

        if not user:
            return Response(_("Invalid %s or password" % User.USERNAME_FIELD), status=status.HTTP_400_BAD_REQUEST)

        return Response(self.serializer_class(user).data, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        request_body=ConfirmEmailAPISerializer(), responses={200: UserAuthOutputSerializer()},
    )
    @action(methods=["POST"], detail=False)
    def confirm_email(self, request):
        serializer = ConfirmEmailSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        token = serializer.fields["token"].instance
        user = token.user
        token.delete()

        user.is_email_verified = True
        user.save()

        return Response(self.serializer_class(request.user).data, status=status.HTTP_200_OK)

    @swagger_auto_schema(responses={200: UserAuthOutputSerializer()},)
    @action(methods=["POST"], detail=False)
    def send_email_confirmation(self, request):
        send_confirm_email(request, request.user)

        return Response(self.serializer_class(request.user).data, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        request_body=ForgotPasswordSerializer(), responses={200: UserAuthOutputSerializer()},
    )
    @action(methods=["POST"], detail=False)
    def forgot_password(self, request):
        serializer = ForgotPasswordSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        user = User.objects.get(email=serializer.validated_data["email"])
        send_forgot_email(request, user)

        return Response(status=status.HTTP_200_OK)

    @swagger_auto_schema(
        request_body=SetNewPasswordAPISerializer(), responses={200: UserAuthOutputSerializer()},
    )
    @action(methods=["POST"], detail=False)
    def set_new_password(self, request):
        serializer = SetNewPasswordSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        token = serializer.fields["token"].instance
        user = token.user
        token.delete()

        user.set_password(serializer.validated_data["password"])
        user.save()

        return Response(self.serializer_class(user).data, status=status.HTTP_200_OK)
