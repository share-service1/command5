from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import action
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from main.models import User
from main.serializers.user import ImageSerializer, UserSerializer


class UserViewSet(GenericViewSet, ListModelMixin, RetrieveModelMixin):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthentication,)

    def get_parsers(self):
        try:
            _action = self.action
        except AttributeError:
            return super().get_parsers()

        if _action in ("avatar",):
            return [
                MultiPartParser,
            ]

        return super().get_parsers()

    def get_serializer_class(self):
        try:
            _action = self.action
        except AttributeError:
            return super().get_serializer_class()

        if _action in ("avatar",):
            return ImageSerializer

        return super().get_serializer_class()

    @swagger_auto_schema(
        operation_id="user_view", responses={200: UserSerializer()},
    )
    def list(self, request, *args, **kwargs):
        """
        Return user data
        ---
        """
        return Response(self.serializer_class(self.request.user).data)

    @swagger_auto_schema(operation_id="image_update", responses={200: UserSerializer()}, request_body=ImageSerializer())
    @action(methods=["POST"], detail=False)
    def avatar(self, request, *args, **kwargs):
        user = request.user
        serialized = self.get_serializer_class()(user, data=request.data, partial=True)
        serialized.is_valid(raise_exception=True)
        serialized.save()
        return Response(self.serializer_class(user).data, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        operation_id="user_update", responses={200: UserSerializer()},
    )
    def partial_update(self, request, pk, *args, **kwargs):
        """
        Update user data
        ---
        """
        user = self.get_object()

        if user != request.user:
            return Response(status=status.HTTP_403_FORBIDDEN)

        serialized = self.serializer_class(user, data=request.data, partial=True)
        if serialized.is_valid():
            serialized.save()
            return Response(self.serializer_class(user).data, status=status.HTTP_200_OK)
        return Response(serialized.errors, status=status.HTTP_400_BAD_REQUEST)
