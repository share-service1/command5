from drf_yasg.utils import swagger_auto_schema
from rest_framework import permissions, status
from rest_framework.mixins import CreateModelMixin, DestroyModelMixin
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from main.models import Rating
from main.serializers.rating import RatingSerializer


# TODO: afters every action return user
class RatingViewSet(
    GenericViewSet, CreateModelMixin, DestroyModelMixin,
):
    serializer_class = RatingSerializer
    queryset = Rating.objects.all()
    permission_classes = (permissions.IsAuthenticated,)

    def perform_destroy(self, instance):
        if self.request.user != instance.who:
            return Response(status=status.HTTP_403_FORBIDDEN)
        super(RatingViewSet, self).perform_destroy(instance)

    @swagger_auto_schema(
        request_body=RatingSerializer(), responses={200: RatingSerializer()},
    )
    def partial_update(self, request, pk, *args, **kwargs):
        rating = self.get_object()

        if rating.who != request.user:
            return Response(status=status.HTTP_403_FORBIDDEN)

        serialized = self.serializer_class(rating, data=request.data, partial=True)
        serialized.is_valid(raise_exception=True)
        serialized.save()

        return Response(self.serializer_class(rating).data, status=status.HTTP_200_OK)
