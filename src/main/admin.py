from django.contrib import admin

from goal.models import Goal, GoalCategory, GoalParty, GoalPartyRequest
from main.models import Token, User

admin.site.register(User)
admin.site.register(Token)
admin.site.register(Goal)
admin.site.register(GoalParty)
admin.site.register(GoalPartyRequest)
admin.site.register(GoalCategory)
