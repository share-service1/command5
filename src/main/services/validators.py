from django.contrib.auth import password_validation
from django.core import exceptions
from django.utils.translation import gettext_lazy as _
from rest_framework import serializers

from main.models import User


def validate_email_existing(email):
    if not User.objects.filter(email=email).exists():
        raise serializers.ValidationError(_("No account found with this email"))


def validate_password(password, user_data):
    user = User(**user_data)

    try:
        password_validation.validate_password(password, user)
    except exceptions.ValidationError as e:
        raise serializers.ValidationError(dict(password=list(e.messages)))
