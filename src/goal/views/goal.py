from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from django.db.models import F, Q
from drf_yasg.openapi import IN_QUERY, Parameter
from drf_yasg.utils import no_body, swagger_auto_schema
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.generics import get_object_or_404
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from goal.models import Goal, GoalCategory, GoalParty, GoalPartyRequest, PaymentByCard
from goal.serializers.goal import (
    GoalCategorySerializer,
    GoalDetailedSerializer,
    GoalPartyDetailedSerializer,
    GoalPartyRequestSerializer,
    GoalPartySerializer,
    PaymentRequestSerializer,
    PaymentResponseSerializer,
)

YOU_ARE_ALREADY_ATTENDING_A_PARTY_FOR_THIS_GOAL = "YOU_ARE_ALREADY_ATTENDING_A_PARTY_FOR_THIS_GOAL"
YOU_HAVE_ALREADY_SUBMITTED_A_REQUEST_TO_JOIN_THIS_GOAL = "YOU_HAVE_ALREADY_SUBMITTED_A_REQUEST_TO_JOIN_THIS_GOAL"
YOU_ARE_NOT_MEMBER_OF_THIS_GOAL_PARTY = "YOU_ARE_NOT_MEMBER_OF_THIS_GOAL_PARTY"


class GoalViewSet(RetrieveModelMixin, ListModelMixin, GenericViewSet):
    serializer_class = GoalDetailedSerializer
    queryset = Goal.objects.all()
    permission_classes = (IsAuthenticatedOrReadOnly,)

    @swagger_auto_schema(manual_parameters=[Parameter("category_id", IN_QUERY, type="integer")])
    def list(self, request, *args, **kwargs):
        """
        Описание: Возвращает все Goal по данной категории
        ---
        """
        queryset = self.filter_queryset(self.get_queryset())
        category_id = self.request.query_params.get("category_id", 0)  # gets the value of the query field

        if category_id:
            queryset = queryset.filter(category_id=int(category_id))
        page = self.paginate_queryset(queryset)

        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(methods=["GET"], detail=False)
    def categories(self, request, *args, **kwargs):
        """
        Возвращает все категории
        ---
        """
        queryset = GoalCategory.objects.all()
        serializer = GoalCategorySerializer(queryset, many=True, context=self.get_serializer_context())
        return Response(serializer.data)

    @swagger_auto_schema(
        request_body=no_body, responses={200: GoalPartyRequestSerializer()},
    )
    @action(methods=["POST"], detail=True)
    def join(self, request, *args, **kwargs):
        """
        Создаёт GoalPartyRequest для goal
        ---
        """
        goal = self.get_object()
        user = request.user
        request_exists = GoalPartyRequest.objects.filter(user=user, goal=goal).exists()

        if request_exists:
            return Response(
                [YOU_HAVE_ALREADY_SUBMITTED_A_REQUEST_TO_JOIN_THIS_GOAL], status=status.HTTP_400_BAD_REQUEST
            )

        is_member_or_admin = GoalParty.objects.filter(Q(members=user) | Q(admin=user), goal=goal).exists()

        if is_member_or_admin:
            return Response([YOU_ARE_ALREADY_ATTENDING_A_PARTY_FOR_THIS_GOAL], status=status.HTTP_400_BAD_REQUEST)

        party_request = GoalPartyRequest.objects.create(user=user, goal=goal)
        s = GoalPartyRequestSerializer(party_request)
        return Response(s.data)

    @swagger_auto_schema(
        request_body=no_body, responses={200: GoalPartySerializer(),},
    )
    @action(methods=["POST"], detail=True, url_path=r"link/(?P<link>\w+)")
    def link(self, request, link, *args, **kwargs):
        """
        Обработка присоединения к GoalParty через ссылку
        ---
        """
        user = request.user
        if user.is_authenticated:
            try:
                goal_party = GoalParty.objects.get(link=link)
                ban_list = goal_party.ban.all()
                members = goal_party.members.all()
                count = members.count()
                if goal_party.goal.max_number_of_members != count and user not in members and user not in ban_list:
                    goal_party.members.add(user)
                    goal_party.save()
                    return Response(status=status.HTTP_200_OK)
                return Response(status=status.HTTP_406_NOT_ACCEPTABLE)
            except ObjectDoesNotExist:
                return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

    @swagger_auto_schema(
        manual_parameters=[Parameter("public", IN_QUERY, type="boolean")],
        request_body=no_body,
        responses={200: GoalPartySerializer(),},
    )
    @action(methods=["POST"], detail=True)
    def party(self, request, *args, **kwargs):
        """
        Создаёт новую GoalParty по инициативе пользователя
        ---
        """
        user = request.user
        if user.is_authenticated:
            goal = self.get_object()
            public = self.request.query_params.get("public")
            if public == "true":
                public = True
            else:
                public = False
            party = GoalParty(admin=user, goal=goal, public=public)
            party.save()
            return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_401_UNAUTHORIZED)

    @swagger_auto_schema(
        manual_parameters=[Parameter("party_id", IN_QUERY, type="integer")],
        request_body=no_body,
        responses={200: GoalPartySerializer(),},
    )
    @action(methods=["POST"], detail=True)
    def link_get(self, request, *args, **kwargs):
        """
        Возвращает invite link
        ---
        """
        user = request.user
        party_id = self.request.query_params.get("party_id", "")

        party = GoalParty.objects.get(pk=party_id)
        members = party.members.all()
        admin = party.admin
        if user.is_authenticated:
            if user in members or user in admin:
                link = party.link
                return Response(status=status.HTTP_200_OK, data="mircod.xyz/goal/{id}/join/" + link)
            return Response(status=status.HTTP_403_FORBIDDEN)
        return Response(status=status.HTTP_401_UNAUTHORIZED)


class GoalPartyViewSet(GenericViewSet):
    serializer_class = GoalPartyDetailedSerializer
    queryset = GoalParty.objects.all()
    permission_classes = (IsAuthenticatedOrReadOnly,)

    @swagger_auto_schema(
        request_body=PaymentRequestSerializer(), responses={200: PaymentResponseSerializer()},
    )
    @action(methods=["POST"], detail=True)
    def pay(self, request, *args, **kwargs):
        serializer = PaymentRequestSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        amount = serializer.validated_data["amount"]
        goal_party_id = self.kwargs["pk"]
        user = request.user

        if not user.goalparty_set.filter(pk=goal_party_id).exists():
            return Response([YOU_ARE_NOT_MEMBER_OF_THIS_GOAL_PARTY], status=status.HTTP_400_BAD_REQUEST)

        with transaction.atomic():
            goal_party = get_object_or_404(self.get_queryset().select_for_update(), pk=goal_party_id)
            payment = PaymentByCard.objects.create(goal_party=goal_party, user=user, amount=amount)
            goal_party.balance = F("balance") + amount
            goal_party.save()

        s = PaymentResponseSerializer(payment)
        return Response(s.data)
