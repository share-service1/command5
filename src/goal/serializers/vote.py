from django.utils.translation import gettext_lazy as _
from rest_framework import serializers
from rest_framework.serializers import ValidationError

from goal.models import Voice, Vote


class VoteCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vote
        fields = (
            "against",
            "comment",
            "party",
        )

    # Move to view and 403?
    def validate_against(self, value):
        if self.context["request"].user == value:
            raise ValidationError(_("You cannot create kick vote against yourself"))

        return value

    # Move to view and 403?
    # def validate(self, attrs):
    #     validated_data = super(VoteCreateSerializer, self).validate(attrs)
    #
    #     party = GoalParty.objects.get(self.initial_data["party"])
    #
    #     if not party.has_active_subscription:
    #         raise ValidationError(_("You cannot create kick vote when subscription still exists"))
    #
    #     return validated_data

    def create(self, validated_data):
        validated_data["initiator"] = self.context["request"].user

        return super().create(validated_data)


class VoteRetrieveSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vote
        fields = "__all__"


class VoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Voice
        fields = "__all__"

    def validate_voter(self, value):
        vote = Vote.objects.get(id=self.initial_data["vote"])
        if vote.against == value:
            raise ValidationError(_("You cannot vote cause it's a vote against you"))

        return value
