from django.db.models import Q
from rest_framework import serializers

from goal.models import Goal, GoalCategory, GoalParty, GoalPartyRequest, PaymentByCard
from main.serializers.user import UserSerializer


class GoalCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = GoalCategory
        fields = ("id", "title", "image")


class GoalPartySerializer(serializers.ModelSerializer):
    class Meta:
        model = GoalParty
        fields = ("id", "admin", "members", "link", "goal")


class GoalDetailedSerializer(serializers.ModelSerializer):
    category = GoalCategorySerializer()
    my_goal_party = serializers.SerializerMethodField()

    def get_my_goal_party(self, obj):
        if self.context["request"].user.is_authenticated:
            goal_party = GoalParty.objects.filter(
                Q(members=self.context["request"].user) | Q(admin=self.context["request"].user), goal=obj
            ).first()

            if goal_party:
                return GoalPartySerializer(goal_party).data

    class Meta:
        model = Goal
        fields = ("id", "title", "description", "image", "max_number_of_members", "category", "my_goal_party")


class GoalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Goal
        fields = (
            "id",
            "title",
            "description",
            "image",
        )


class GoalPartyRequestSerializer(serializers.ModelSerializer):
    goal = GoalSerializer()
    user = UserSerializer()

    class Meta:
        model = GoalPartyRequest
        fields = ("id", "goal", "user")


class GoalPartyDetailedSerializer(serializers.ModelSerializer):
    goal = GoalSerializer()

    class Meta:
        model = GoalParty
        fields = ("id", "goal", "balance", "members")


class PaymentResponseSerializer(serializers.ModelSerializer):
    class Meta:
        model = PaymentByCard
        fields = ("id", "goal_party", "user", "amount")


class PaymentRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = PaymentByCard
        fields = ("amount",)
