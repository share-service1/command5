import factory
from factory.django import DjangoModelFactory

from goal.models import GoalCategory


class GoalCategoryFactory(DjangoModelFactory):
    title = factory.Faker("name")
    image = factory.django.ImageField(color="blue")

    class Meta:
        model = GoalCategory
