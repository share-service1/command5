from rest_framework.routers import DefaultRouter

from goal.views import GoalPartyViewSet, GoalViewSet, VoteViewSet

router = DefaultRouter(trailing_slash=True)
router.register("goal", GoalViewSet, basename="goal")
router.register("goal_party", GoalPartyViewSet, basename="goal_party")
router.register("vote", VoteViewSet, basename="vote")

goal_urls = [
    *router.urls,
]
