from django_counter_cache_field import connect_counter

from goal.models import Voice

connect_counter("for_voice_count", Voice.vote, lambda instance: instance.for_or_against)
connect_counter("against_voice_count", Voice.vote, lambda instance: not instance.for_or_against)
