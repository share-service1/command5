from django.contrib import admin

from goal.models import Voice, Vote

admin.site.register(Vote)
admin.site.register(Voice)
