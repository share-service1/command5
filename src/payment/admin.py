from django.contrib import admin

from payment.models import SubscriptionPaymentHistory, UserPaymentHistory

admin.site.register(SubscriptionPaymentHistory)
admin.site.register(UserPaymentHistory)
