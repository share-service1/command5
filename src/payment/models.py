from django.contrib.auth import get_user_model
from django.db import models

from goal.models import GoalParty

User = get_user_model()


class SubscriptionPaymentHistory(models.Model):
    party = models.ForeignKey(GoalParty, on_delete=models.SET_NULL, related_name="subscriptions", null=True)
    amount = models.DecimalField(max_digits=7, decimal_places=2)
    payment_date = models.DateTimeField(null=True, blank=True)
    start_date = models.DateTimeField(null=True)
    end_date = models.DateTimeField(null=True)


class UserPaymentHistory(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    amount = models.DecimalField(max_digits=7, decimal_places=2)
    subscription = models.ForeignKey(
        SubscriptionPaymentHistory, on_delete=models.SET_NULL, null=True, related_name="payers"
    )
    payment_date = models.DateTimeField(null=True, blank=True)
