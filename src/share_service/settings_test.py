from share_service.settings import *  # noqa

DATABASES = {"default": {"ENGINE": "django.db.backends.sqlite3", "NAME": BASE_DIR / "db.sqlite3",}}  # noqa
DEFAULT_FILE_STORAGE = "django.core.files.storage.FileSystemStorage"
MEDIA_URL = "/media/"  # noqa
MEDIA_ROOT = os.path.join(BASE_DIR, "media/")  # noqa
