import os

from celery import Celery
from celery.schedules import crontab

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "share_service.settings")

app = Celery("share_service")

app.config_from_object("django.conf:settings", namespace="CELERY")

app.conf.beat_schedule = {
    "task_goal_party_link": {"task": "goal.tasks.goal_party_request", "schedule": crontab(minute="*/4"), "args": None,},
    "task_goal_party_link_update": {
        "task": "goal.tasks.goal_party_link_update",
        "schedule": crontab(hour=0, minute=0, day_of_week=5),
        "args": None,
    },
}

app.autodiscover_tasks()


@app.task(bind=True)
def debug_task(self):
    print(f"Request: {self.request!r}")
