#!/bin/sh

wait_for() {
  NAME=$1
  HOST=$2
  PORT=$3

  echo "Waiting for $NAME..."

  while ! nc -z $HOST $PORT; do
    sleep 0.1
  done

  echo "$NAME has started"
}

if [ "$DATABASE" = "postgres" ]
then
  wait_for "Postgres" $DB_HOST $DB_PORT
fi


python manage.py migrate
python manage.py collectstatic --noinput --clear
django-admin compilemessages

exec "$@"
